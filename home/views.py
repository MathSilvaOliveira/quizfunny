from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import *
import random


def home(request):
    context = {'categorias' : Categoria.objects.all()}

    if request.GET.get('categoria'):
        return redirect(f"/quiz/?categoria={request.GET.get('categoria')}")

    return render (request, 'home.html' , context)


def quiz(request):
    context = {'categoria' : request.GET.get('categoria')}
    return render(request , 'quiz.html' , context)

def get_quiz(request):
    try:
        questao_objs = Questoes.objects.all()

        if request.GET.get('categoria'):
            questao_objs = questao_objs.filter(categoria__categoria_nome__icontains=request.GET.get('categoria'))

        questao_objs = list(questao_objs)
        data = []
        random.shuffle(questao_objs)
        
        for questao_objs in questao_objs:
            data.append({
                "uid" : questao_objs.uid,
                "categoria": questao_objs.categoria.categoria_nome,
                "questao": questao_objs.questao,
                "marks": questao_objs.marks ,
                'resposta' : questao_objs.get_respostas()   
            })
                
        payload = {'status' : True ,'data' : data}
        
        return JsonResponse(payload)
              
    except Exception as e:
        print(e)
    return HttpResponse("Algo deu errado")        
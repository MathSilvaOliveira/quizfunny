from django.db import models
import uuid
import random

class BaseModel(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    criado_em = models.DateField(auto_now_add=True)
    atualizado_em = models.DateField(auto_now_add=True)
    
    class Meta:
        abstract = True
    
class Categoria(BaseModel):
    categoria_nome = models.CharField(max_length=100)
    
    def __str__(self) -> str:
        return self.categoria_nome


class Questoes(BaseModel):
    categoria = models.ForeignKey(Categoria ,  related_name='categoria', on_delete=models.CASCADE)
    questao = models.CharField(max_length=100)
    marks = models.IntegerField(default=5)
    
    def __str__(self) -> str:
        return self.questao
    
    def get_respostas(self):
        resposta_objs = list(Resposta.objects.filter(questao = self))
        random.shuffle(resposta_objs)
        data = []
        for resposta_obj in resposta_objs:
            data.append({
                'resposta' : resposta_obj.resposta,
                'is_correta' : resposta_obj.is_correct
            })
        return data
    
class Resposta(BaseModel):
    questao = models.ForeignKey(Questoes , related_name='questao_resposta',  on_delete=models.CASCADE)    
    resposta = models.CharField(max_length=100)
    is_correct = models.BooleanField(default=False)
    
    def __str__(self) -> str:
        return self.resposta
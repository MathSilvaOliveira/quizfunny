from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Categoria)

class RespostaAdmin(admin.StackedInline):
    model = Resposta
    
class QuestoesAdmin(admin.ModelAdmin):
    inlines = [RespostaAdmin]    
    
    
admin.site.register(Questoes, QuestoesAdmin)
admin.site.register(Resposta)